//
//  ResponseError.swift
//  SIMVVM
//
//  Created by Sarvesh Doshi on 17/06/23.
//

import Foundation

struct ResponseError: Error, Decodable {

    let responseMessage: String?
    let responseCode: Int

    enum CodingKeys: String, CodingKey {
        case responseMessage = "message"
        case responseCode = "status"
    }

}
