//
//  NetworkingController.swift
//  SIMVVM
//
//  Created by Sarvesh Doshi on 17/06/23.
//

import Foundation

class NetworkingController {
    
    static let shared: NetworkingController = .init()

    private let decoder = JSONDecoder()

    private init() {}

    @discardableResult
    func post<R: POSTRequestModel>(
        _ requestModel: R,
        receiveCompletionOn queue: DispatchQueue = .main,
        completion: @escaping (Result<R.POSTResponse, Error>) -> Void
    ) -> RequestToken? {
        let completion: (Result<R.POSTResponse, Error>) -> Void = { result in
            queue.async { completion(result) }
        }
        guard let urlRequest = requestModel.postURLRequest else {
            completion(.failure(URLError(.unknown)))
            return nil
        }
        return request(urlRequest, completion: completion)
    }

    @discardableResult
    func put<R: PUTRequestModel>(
        _ requestModel: R,
        receiveCompletionOn queue: DispatchQueue = .main,
        completion: @escaping (Result<R.PUTResponse, Error>) -> Void
    ) -> RequestToken? {
        let completion: (Result<R.PUTResponse, Error>) -> Void = { result in
            queue.async { completion(result) }
        }
        guard let urlRequest = requestModel.putURLRequest else {
            completion(.failure(URLError(.unknown)))
            return nil
        }
        return request(urlRequest, completion: completion)
    }
    
    @discardableResult
    func get<R: GETRequestModel>(
        _ requestModel: R,
        receiveCompletionOn queue: DispatchQueue = .main,
        completion: @escaping (Result<R.GETResponse, Error>) -> Void
    ) -> RequestToken? {
        let completion: (Result<R.GETResponse, Error>) -> Void = { result in
            queue.async { completion(result) }
        }
        guard let urlRequest = requestModel.getURLRequest else {
            completion(.failure(URLError(.unknown)))
            return nil
        }
        return request(urlRequest, completion: completion)
    }

    func request<R: ResponseModel>(
        _ request: URLRequest,
        completion: @escaping (Result<R, Error>) -> Void
    ) -> RequestToken {
        let decoder = self.decoder

        var request = request
//        tokenKey.map { request.setValue($0, forHTTPHeaderField: "TokenKey") }
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        let task = URLSession.shared.dataTask(
            with: request
        ) { [weak self] data, response, error in

            #if DEBUG

            self?.log(request, response, data, error)

            #endif

            if let error = error {
                completion(.failure(error))
                return
            }

            guard let data = data else {
                completion(.failure(URLError(.unknown)))
                return
            }
            let result = Result<R, Error> {
                if let response = try? decoder.decode(R.self, from: data) //,response.isSuccess
                { return response }

                if let error = try? decoder
                    .decode(ResponseError.self, from: data)
                { throw error }

                throw URLError(.badServerResponse)
            }
            if
                case .failure(let error as ResponseError) = result,
                (512...514).contains(error.responseCode)
            {
                completion(result)
            } else {
                completion(result)
            }
        }
        task.resume()
        
        return RequestToken(task: task)
    }

    #if DEBUG

    private func log(
        _ request: URLRequest,
        _ response: URLResponse?,
        _ data: Data?,
        _ error: Error?
    ) {
        var logString = "--------"

        if
            let method = request.httpMethod,
            let url = request.url
        {
            logString += "\n\n\(method) \(url)"
        }

        request.allHTTPHeaderFields.map { headers in
            logString += "\n\nRequest Headers:\n\n" + headers
                .map { "\($0): \($1)" }
                .joined(separator: "\n")
        }

        response
            .flatMap { ($0 as? HTTPURLResponse)?.allHeaderFields }
            .map { headers in
                logString += "\n\nResponse Headers:\n\n" + headers
                    .map { "\($0): \($1)" }
                    .joined(separator: "\n")
            }

        error.map {
            logString += "\n\nError:\n\n\($0.localizedDescription)"
        }

        data
            .flatMap { String(data: $0, encoding: .utf8) }
            .map { logString += "\n\nData:\n\n\($0)\n\n" }

        logString += "--------\n\n"

        print(logString)
    }

    #endif

}
