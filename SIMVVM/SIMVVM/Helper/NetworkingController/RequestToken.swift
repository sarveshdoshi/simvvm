//
//  RequestToken.swift
//  SIMVVM
//
//  Created by Sarvesh Doshi on 17/06/23.
//

import Foundation

final class RequestToken {
    private weak var task: URLSessionDataTask?

    init(task: URLSessionDataTask) {
        self.task = task
    }

    func cancel() {
        task?.cancel()
    }
}
