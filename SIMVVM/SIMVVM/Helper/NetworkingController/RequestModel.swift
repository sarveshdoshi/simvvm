//
//  RequestModel.swift
//  SIMVVM
//
//  Created by Sarvesh Doshi on 17/06/23.
//

import Foundation
/*
 https://demo.sportz.io/nzin01312019187360.json
 https://demo.sportz.io/sapk01222019186652.json
 */
let BASE_URL: String? = "https://demo.sportz.io/" // BASE_URL
//let BASE_URL: String? = "https://demo.sportz.io/" // BASE_URL

protocol RequestModel: Encodable {

    var path: String { get }

}

protocol GETRequestModel: RequestModel {

    associatedtype GETResponse: ResponseModel

    var getURLRequest: URLRequest? { get }

}

extension GETRequestModel {

    var getURLRequest: URLRequest? {

        guard var components = BASE_URL.flatMap({ URLComponents(string: $0) })
        else { return nil }

        components.path += path

        guard var request = components.url.map({ URLRequest(url: $0) })
        else { return nil }

        request.httpMethod = "GET"
        return request
    }

}

protocol POSTRequestModel: RequestModel {

    associatedtype POSTResponse: ResponseModel

    var postURLRequest: URLRequest? { get }

}

extension POSTRequestModel {

    var postURLRequest: URLRequest? {

        guard var components = BASE_URL.flatMap({ URLComponents(string: $0) })
        else { return nil }

        components.path += path

        guard var request = components.url.map({ URLRequest(url: $0) })
        else { return nil }

        request.httpBody = try? JSONEncoder().encode(self)
        request.httpMethod = "POST"
        return request
    }

}

protocol PUTRequestModel: RequestModel {

    associatedtype PUTResponse: ResponseModel

    var putURLRequest: URLRequest? { get }

}

extension PUTRequestModel {

    var putURLRequest: URLRequest? {


        guard var components = BASE_URL.flatMap({ URLComponents(string: $0) })
        else { return nil }

        components.path += path

        guard var request = components.url.map({ URLRequest(url: $0) })
        else { return nil }

        request.httpBody = try? JSONEncoder().encode(self)
        request.httpMethod = "PUT"
        return request
    }

}

protocol ResponseModel: Decodable {
  
}
