//
//  UIViewController + Alerts.swift
//  SIMVVM
//
//  Created by Sarvesh Doshi on 17/06/23.
//

import UIKit


extension UIViewController {

    func showAlert(title: String = "Alert",
                   message: String) {

        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok",
                                      style: .default,
                                      handler: nil))
        self.present(alert, animated: true, completion: nil)

    }
    func showAlert(title: String = "Alert",
                   message: String,
                   completionHandler  onComplete:@escaping (Bool) -> Void) {

        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler:{
            _ in
            onComplete(true)
        })
        )
        self.present(alert, animated: true, completion: nil)
    }

    //custom Alert
    func showMessage(title: String = "Alert",
                     message: String,
                     positiveTitle: String,
                     negativeTitle: String = "",
                     onPositive: (() -> Void)? = nil,
                     onNegative: (() -> Void)? = nil) {

        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)

        if !negativeTitle.isEmpty {
            alert.addAction(UIAlertAction(title: negativeTitle, style: .destructive, handler: { [weak alert](_) in
                onNegative?()
                alert?.dismiss(animated: true, completion: nil)
            }))
        }


        if !positiveTitle.isEmpty {
            alert.addAction(UIAlertAction(title: positiveTitle, style: .default, handler: { [weak alert](_) in
                onPositive?()
                alert?.dismiss(animated: true, completion: nil)
            }))
        }

        present(alert, animated: true, completion: nil)
    }
}
