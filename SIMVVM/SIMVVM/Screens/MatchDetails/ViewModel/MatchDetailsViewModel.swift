//
//  MatchDetailsViewModel.swift
//  SIMVVM
//
//  Created by Sarvesh Doshi on 17/06/23.
//

import Foundation


final class MatchDetailsViewModel {
    
    var response: MatchDetailsModelResponse?
    var eventHandler: ((_ event: Event) -> Void)?
    var matchDetail: Matchdetail?
    var nuggets: [String]?
    var innings: [Inning]?
    var teams: [String: Team]?
    var notes: [String: [String]]?
    
    lazy var homeTeamPlayers: [EnumeratedSequence<[String : Player]>.Element]? = {
        
        let eumerated = teams?[matchDetail?.teamHome ?? ""]?.players?.enumerated()
        return eumerated?.sorted(by: { (lhs, rhs) -> Bool in
            return lhs.element.key > rhs.element.key
        })
    }()
    lazy var awayTeamPlayers: [EnumeratedSequence<[String : Player]>.Element]? = {
        let eumerated = teams?[matchDetail?.teamAway ?? ""]?.players?.enumerated()
        return eumerated?.sorted(by: { (lhs, rhs) -> Bool in
            return lhs.element.key > rhs.element.key
        })
    }()
    
    func fetchMatchDetails() {
        
        let request = MatchDetailsModelRequest()
        self.eventHandler?(.loading)
        NetworkingController.shared.get(request) { [weak self] (result) in
            guard let self else { return }
            self.eventHandler?(.stopLoading)
            switch result {
            case .success(let response):
                print("Success")
                self.response = response
                self.matchDetail = response.matchdetail
                self.nuggets = response.nuggets
                self.innings = response.innings
                self.teams = response.teams
                self.notes = response.notes
                self.eventHandler?(.dataLoaded)
            case .failure(let error):
                self.eventHandler?(.error(error))
                print(error.localizedDescription)
            }
        }
    }
    
    
}
