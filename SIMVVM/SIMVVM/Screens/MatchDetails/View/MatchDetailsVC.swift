//
//  MatchDetailsVC.swift
//  SIMVVM
//
//  Created by Sarvesh Doshi on 17/06/23.
//

import UIKit

class MatchDetailsVC: UIViewController {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var venueDetailsLabel: UILabel!
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private var viewModel = MatchDetailsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configuration()
    }
    
    @IBAction func segmentValueChange(_ sender: UISegmentedControl) {
        tableView.reloadData()
    }

}


extension MatchDetailsVC {
    
    func configuration() {
        initViewModel()
        observeEvent()
    }
    
    func initViewModel() {
        viewModel.fetchMatchDetails()
    }
    
    func observeEvent() {
        viewModel.eventHandler = { [ weak self ] event in
            guard let self else { return }
            switch event {
          
            case .loading:
                self.activityIndicator.startAnimating()
            case .stopLoading:
                self.activityIndicator.stopAnimating()
            case .dataLoaded:
                print("Api Parsing is done.")
                DispatchQueue.main.async {
                    self.loadUI()
                    self.setUpSegementController()
                    self.setupTableView()
                }
            case .error(let error):
                print("Some Error occured. -> \(error?.localizedDescription ?? "")")
                self.showMessage(title: "Error",
                                 message: "Something went wrong at the moment",
                                 positiveTitle: "Retry",
                                 negativeTitle: "Cancel") {
                    self.viewModel.fetchMatchDetails()
                } onNegative: {
                    self.dismiss(animated: true)
                }

            }
        }
    }
    
    func loadUI() {
        self.dateLabel.text = "Date: \(self.viewModel.matchDetail?.match?.date ?? "")"
        self.timeLabel.text = "Time: \(self.viewModel.matchDetail?.match?.time ?? "")"
        self.venueDetailsLabel.text = "Venue: \(self.viewModel.matchDetail?.venue?.name ?? "")"
    }
    
    func setUpSegementController() {
        let homeTeam = viewModel.teams?[viewModel.matchDetail?.teamHome ?? ""]?.nameFull
        let awayTeam = viewModel.teams?[viewModel.matchDetail?.teamAway ?? ""]?.nameFull
        segmentController.setTitle(homeTeam ?? "Team A", forSegmentAt: 0)
        segmentController.setTitle(awayTeam ?? "Team B", forSegmentAt: 1)
    }
    
    func setupTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
    }
}

extension MatchDetailsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch segmentController.selectedSegmentIndex
        {
        case 0:
            return viewModel.homeTeamPlayers?.count ?? 0
        case 1:
            return viewModel.awayTeamPlayers?.count ?? 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        
        switch segmentController.selectedSegmentIndex
        {
        case 0:
            
            let playerDetails = viewModel.homeTeamPlayers?[indexPath.row].element.value
            cell.textLabel?.text = "\(playerDetails?.nameFull ?? "") \(playerDetails?.iscaptain ?? false ? "(C)" : "") \(playerDetails?.iskeeper ?? false ? "(Wk)" : "")"
        case 1:
            let playerDetails = viewModel.awayTeamPlayers?[indexPath.row].element.value
            cell.textLabel?.text = "\(playerDetails?.nameFull ?? "") \(playerDetails?.iscaptain ?? false ? "(C)" : "") \(playerDetails?.iskeeper ?? false ? "(Wk)" : "")"
        default:
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(56)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Team Players List"
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(32)
    }
}
