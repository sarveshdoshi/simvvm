//
//  MatchDetailsModel.swift
//  SIMVVM
//
//  Created by Sarvesh Doshi on 17/06/23.
//

import Foundation

// Request Model
struct MatchDetailsModelRequest: GETRequestModel {
    typealias GETResponse = MatchDetailsModelResponse
    
    var path: String { "nzin01312019187360.json" }
//    var path: String { "sapk01222019186652.json" }
    
}

// Response Model
struct MatchDetailsModelResponse: ResponseModel {
    
    var matchdetail: Matchdetail?
    var nuggets: [String]?
    var innings: [Inning]?
    var teams: [String: Team]?
    var notes: [String: [String]]?

    enum CodingKeys: String, CodingKey {
        case matchdetail = "Matchdetail"
        case nuggets = "Nuggets"
        case innings = "Innings"
        case teams = "Teams"
        case notes = "Notes"
    }
}

// MARK: - Inning
struct Inning: Codable {
    var number, battingteam, total, wickets: String?
    var overs, runrate, byes, legbyes: String?
    var wides, noballs, penalty, allottedOvers: String?
    var batsmen: [InningBatsman]?
    var partnershipCurrent: PartnershipCurrent?
    var bowlers: [Bowler]?
    var fallofWickets: [FallofWicket]?
    var powerPlay: PowerPlay?
    var target: String?

    enum CodingKeys: String, CodingKey {
        case number = "Number"
        case battingteam = "Battingteam"
        case total = "Total"
        case wickets = "Wickets"
        case overs = "Overs"
        case runrate = "Runrate"
        case byes = "Byes"
        case legbyes = "Legbyes"
        case wides = "Wides"
        case noballs = "Noballs"
        case penalty = "Penalty"
        case allottedOvers = "AllottedOvers"
        case batsmen = "Batsmen"
        case partnershipCurrent = "Partnership_Current"
        case bowlers = "Bowlers"
        case fallofWickets = "FallofWickets"
        case powerPlay = "PowerPlay"
        case target = "Target"
    }
}

// MARK: - InningBatsman
struct InningBatsman: Codable {
    var batsman, runs, balls, fours: String?
    var sixes, dots, strikerate, dismissal: String?
    var howout, bowler, fielder: String?
    var isonstrike: Bool?

    enum CodingKeys: String, CodingKey {
        case batsman = "Batsman"
        case runs = "Runs"
        case balls = "Balls"
        case fours = "Fours"
        case sixes = "Sixes"
        case dots = "Dots"
        case strikerate = "Strikerate"
        case dismissal = "Dismissal"
        case howout = "Howout"
        case bowler = "Bowler"
        case fielder = "Fielder"
        case isonstrike = "Isonstrike"
    }
}

// MARK: - Bowler
struct Bowler: Codable {
    var bowler, overs, maidens, runs: String?
    var wickets, economyrate, noballs, wides: String?
    var dots: String?
    var isbowlingtandem, isbowlingnow: Bool?
    var thisOver: [ThisOver]?

    enum CodingKeys: String, CodingKey {
        case bowler = "Bowler"
        case overs = "Overs"
        case maidens = "Maidens"
        case runs = "Runs"
        case wickets = "Wickets"
        case economyrate = "Economyrate"
        case noballs = "Noballs"
        case wides = "Wides"
        case dots = "Dots"
        case isbowlingtandem = "Isbowlingtandem"
        case isbowlingnow = "Isbowlingnow"
        case thisOver = "ThisOver"
    }
}

// MARK: - ThisOver
struct ThisOver: Codable {
    var t, b: String?

    enum CodingKeys: String, CodingKey {
        case t = "T"
        case b = "B"
    }
}

// MARK: - FallofWicket
struct FallofWicket: Codable {
    var batsman, score, overs: String?

    enum CodingKeys: String, CodingKey {
        case batsman = "Batsman"
        case score = "Score"
        case overs = "Overs"
    }
}

// MARK: - PartnershipCurrent
struct PartnershipCurrent: Codable {
    var runs, balls: String?
    var batsmen: [PartnershipCurrentBatsman]?

    enum CodingKeys: String, CodingKey {
        case runs = "Runs"
        case balls = "Balls"
        case batsmen = "Batsmen"
    }
}

// MARK: - PartnershipCurrentBatsman
struct PartnershipCurrentBatsman: Codable {
    var batsman, runs, balls: String?

    enum CodingKeys: String, CodingKey {
        case batsman = "Batsman"
        case runs = "Runs"
        case balls = "Balls"
    }
}

// MARK: - PowerPlay
struct PowerPlay: Codable {
    var pp1, pp2: String?

    enum CodingKeys: String, CodingKey {
        case pp1 = "PP1"
        case pp2 = "PP2"
    }
}

// MARK: - Matchdetail
struct Matchdetail: Codable {
    var teamHome, teamAway: String?
    var match: Match?
    var series: Series?
    var venue: Venue?
    var officials: Officials?
    var weather, tosswonby, status, statusID: String?
    var playerMatch, result, winningteam, winmargin: String?
    var equation: String?

    enum CodingKeys: String, CodingKey {
        case teamHome = "Team_Home"
        case teamAway = "Team_Away"
        case match = "Match"
        case series = "Series"
        case venue = "Venue"
        case officials = "Officials"
        case weather = "Weather"
        case tosswonby = "Tosswonby"
        case status = "Status"
        case statusID = "Status_Id"
        case playerMatch = "Player_Match"
        case result = "Result"
        case winningteam = "Winningteam"
        case winmargin = "Winmargin"
        case equation = "Equation"
    }
}

// MARK: - Match
struct Match: Codable {
    var livecoverage, id, code, league: String?
    var number, type, date, time: String?
    var offset, daynight: String?

    enum CodingKeys: String, CodingKey {
        case livecoverage = "Livecoverage"
        case id = "Id"
        case code = "Code"
        case league = "League"
        case number = "Number"
        case type = "Type"
        case date = "Date"
        case time = "Time"
        case offset = "Offset"
        case daynight = "Daynight"
    }
}

// MARK: - Officials
struct Officials: Codable {
    var umpires, referee: String?

    enum CodingKeys: String, CodingKey {
        case umpires = "Umpires"
        case referee = "Referee"
    }
}

// MARK: - Series
struct Series: Codable {
    var id, name, status, tour: String?
    var tourName: String?

    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case name = "Name"
        case status = "Status"
        case tour = "Tour"
        case tourName = "Tour_Name"
    }
}

// MARK: - Venue
struct Venue: Codable {
    var id, name: String?

    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case name = "Name"
    }
}

// MARK: - Team
struct Team: Codable {
    var nameFull, nameShort: String?
    var players: [String: Player]?

    enum CodingKeys: String, CodingKey {
        case nameFull = "Name_Full"
        case nameShort = "Name_Short"
        case players = "Players"
    }
}

// MARK: - Player
struct Player: Codable {
    var position, nameFull: String?
    var iskeeper: Bool?
    var batting: Batting?
    var bowling: Bowling?
    var iscaptain: Bool?

    enum CodingKeys: String, CodingKey {
        case position = "Position"
        case nameFull = "Name_Full"
        case iskeeper = "Iskeeper"
        case batting = "Batting"
        case bowling = "Bowling"
        case iscaptain = "Iscaptain"
    }
}

// MARK: - Batting
struct Batting: Codable {
    var style: Style?
    var average, strikerate, runs: String?

    enum CodingKeys: String, CodingKey {
        case style = "Style"
        case average = "Average"
        case strikerate = "Strikerate"
        case runs = "Runs"
    }
}

enum Style: String, Codable {
    case lhb = "LHB"
    case rhb = "RHB"
}

// MARK: - Bowling
struct Bowling: Codable {
    var style, average, economyrate, wickets: String?

    enum CodingKeys: String, CodingKey {
        case style = "Style"
        case average = "Average"
        case economyrate = "Economyrate"
        case wickets = "Wickets"
    }
}
