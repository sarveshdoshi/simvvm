//
//  LandingPageVC.swift
//  SIMVVM
//
//  Created by Sarvesh Doshi on 17/06/23.
//

import UIKit

class LandingPageVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func screen1Tapped(_ sender: UIButton) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "MatchDetailsVC") as! MatchDetailsVC
        self.navigationController?.pushViewController(vc,
                                                      animated: true)
    }
}
