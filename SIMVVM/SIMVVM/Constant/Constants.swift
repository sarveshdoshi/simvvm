//
//  Constants.swift
//  SIMVVM
//
//  Created by Sarvesh Doshi on 17/06/23.
//

import Foundation

enum Event {
    case loading
    case stopLoading
    case dataLoaded
    case error(Error?)
}
